# {{ title }} - Documentation

> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

```{toctree}
---
caption: Available languages for the guide
maxdepth: 1
---
User guide (English) <en_guide/index>
Guide d'utilisation (français) <fr_guide/index>
```

----

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/packaging
development/testing
development/sponsors
development/history
```

<!-- development/diagram -->
<!-- Code documentation <_apidoc/modules> -->
