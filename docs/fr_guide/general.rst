Objectif du plugin
==================

Aperçu général
--------------

Ce plugin a pour objectif de proposer un moyen simple de vérifier si la structure d'un
jeu de données est conforme avec la structure d'un jeu de données de rérérence.

Les formats supportés sont :

- geopackage
- dossier de shapefile(s)
- DXF

Comment ça marche ?
-------------------

Prérequis
.........

Un jeu de données de référence est requis.

Processus
.........

#. Créer un fichier de configuration de la conformité dans lequel peuvent être choisis plusieurs réglages

   - Vérification du SCR
   - Présence de trous et de courbes
   - Champs
   - Règles de géométrie et de topologie
   - etc.

#. Utiliser ce fichier de configuration et un jeu de données à vérifier pour lancer la Vérification de conformité
#. Analyser le rapport
