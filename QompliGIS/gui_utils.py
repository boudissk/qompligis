"""
All wizard pages classes
"""

from collections import defaultdict
from pathlib import Path
from typing import Any, Dict, Optional

import yaml
from qgis.core import QgsApplication, QgsLayerItem, QgsVectorFileWriter, QgsWkbTypes
from qgis.gui import QgsFileWidget
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import (
    QComboBox,
    QDialog,
    QDialogButtonBox,
    QFormLayout,
    QHBoxLayout,
    QLabel,
    QListWidget,
    QListWidgetItem,
    QMessageBox,
    QStackedWidget,
    QTableWidgetItem,
    QTextEdit,
    QVBoxLayout,
    QWidget,
    QWizard,
    QWizardPage,
)

from QompliGIS.qt_utils import boolToQtCheckState, qtCheckStateToBool, tr
from QompliGIS.report import ReportFormat
from QompliGIS.utils import (
    DoVerif,
    InputFormat,
    check_conf_dict_format,
    get_input_format,
    list_dxf_info,
    list_gpkg_info,
    list_shp_info,
)

ICON_DICT = defaultdict(
    lambda: QgsLayerItem.iconDefault(),
    {
        "POINT": QgsLayerItem.iconPoint(),
        "LINESTRING": QgsLayerItem.iconLine(),
        "POLYGON": QgsLayerItem.iconPolygon(),
        "MULTIPOINT": QgsLayerItem.iconPoint(),
        "MULTILINESTRING": QgsLayerItem.iconLine(),
        "MULTIPOLYGON": QgsLayerItem.iconPolygon(),
        "NOGEOMETRY": QgsLayerItem.iconTable(),
    },
)


class DataInputPage(QWizardPage):
    """
    Page to select the data that will be used as a reference
    to create the configuration file
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Choose the dataset to use as a reference"))
        cbx_input_format = QComboBox()
        cbx_input_format.addItems(
            ["", InputFormat.SHAPEFILES.value, InputFormat.DXF.value]
        )
        if "gpkg" in QgsVectorFileWriter.supportedFormatExtensions():
            cbx_input_format.addItem(InputFormat.GEOPACKAGE.value)
        filepath = QgsFileWidget()
        lbl = QLabel(tr("Choose a file"))

        def textChanged(text: str) -> None:
            """
            Change the behavior or the file selector according to
            the file type
            """
            if text == InputFormat.SHAPEFILES.value:
                filepath.setStorageMode(QgsFileWidget.StorageMode.GetDirectory)
                lbl.setText(tr("Choose a folder"))
            else:
                filepath.setStorageMode(QgsFileWidget.StorageMode.GetFile)
                lbl.setText(tr("Choose a file"))
                filepath.setFilter(text)

        textChanged(cbx_input_format.currentText())
        cbx_input_format.currentTextChanged.connect(textChanged)
        layout = QFormLayout()
        layout.addRow(tr("Format"), cbx_input_format)
        layout.addRow(lbl, filepath)
        self.setLayout(layout)
        self.registerField(
            "input_format*",
            cbx_input_format,
            "currentText",
            cbx_input_format.currentTextChanged,
        )
        self.registerField("filepath*", filepath.lineEdit())

    def validatePage(self):
        """Validate the Page."""
        filepath = Path(self.field("filepath"))
        if not filepath.exists():
            QMessageBox.warning(
                None,
                tr("Warning"),
                tr("The path {0} does not exist!").format(filepath),
            )
            return False
        return True


class VerifDataInputPage(QWizardPage):
    """
    Page to select the data to be checked
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Choose the dataset to check"))
        self.filepath = QgsFileWidget()
        self.lbl = QLabel()
        layout = QFormLayout()
        layout.addRow(self.lbl, self.filepath)
        self.setLayout(layout)
        self.registerField("filepath*", self.filepath.lineEdit())

    def initializePage(self):
        """Initialize the page."""

        input_format = get_input_format(Path(self.field("config_file")))
        if input_format == InputFormat.SHAPEFILES:
            self.filepath.setStorageMode(QgsFileWidget.StorageMode.GetDirectory)
            self.lbl.setText(tr("Choose a folder with your shapefiles"))
        else:
            self.filepath.setStorageMode(QgsFileWidget.StorageMode.GetFile)
            self.lbl.setText(tr("Choose a file - {0}").format(input_format.value))
            self.filepath.setFilter(input_format.value)

    def validatePage(self):
        """Validate the page."""

        path_to_validate = self.filepath.filePath()
        input_format = get_input_format(Path(self.field("config_file")))
        if not Path(path_to_validate).exists():
            QMessageBox.warning(
                None,
                tr("Warning"),
                tr("The path {0} does not exist!").format(path_to_validate),
            )
            return False
        if input_format == InputFormat.SHAPEFILES:
            if not Path(path_to_validate).is_dir():
                QMessageBox.warning(
                    None,
                    tr("Warning"),
                    tr("The path {0} must be a folder!").format(path_to_validate),
                )
                return False
        else:
            if Path(path_to_validate).is_dir():
                QMessageBox.warning(
                    None,
                    tr("Warning"),
                    tr("The path {0} must be a file!").format(path_to_validate),
                )
                return False

        return True


class ConfigEditPage(QWizardPage):
    """
    Page to configure the verification constraints
    while editing an existing configuration
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Constraints configuration"))
        page_layout = QHBoxLayout()
        self.setLayout(page_layout)
        self.stack = QStackedWidget()
        self.layers_view = QListWidget()
        self.layers_view.setMinimumWidth(150)
        self.layers_view.setMaximumWidth(300)
        self.conf_dict: Dict[str, Any] = {}
        page_layout.addWidget(self.layers_view)
        page_layout.addWidget(self.stack)

        self.layers_view.currentRowChanged.connect(self.stack.setCurrentIndex)

    def initializePage(self):
        """Initialize the page."""

        with Path(self.field("config_file")).open() as filein:
            self.conf_dict = yaml.full_load(filein)

        input_format = InputFormat(self.conf_dict["input_format"])
        filepath = self.conf_dict["filepath"]

        # Shapefile
        if input_format == InputFormat.SHAPEFILES:
            info_list = list_shp_info(filepath)

        # CAD or DXF
        elif input_format == InputFormat.DXF:
            info_list = list_dxf_info(filepath)

        # GPKG
        elif input_format == InputFormat.GEOPACKAGE:
            info_list = list_gpkg_info(filepath)

        layers_fields_added_deleted_dict = {}
        for layer, (geom_type, fields) in info_list.items():
            field_dict = self.conf_dict[layer]["fields"]
            layers_fields_added_deleted_dict[layer] = {
                "fields_added": [],
                "fields_deleted": [],
            }
            layer_fields = []
            wid = uic.loadUi(Path(__file__).resolve(True).parent / "conf_wid.ui")
            wid.tbl_fields.setRowCount(len(fields))
            dim = QgsWkbTypes.wkbDimensions(QgsWkbTypes.parseType(geom_type))
            for i, field in enumerate(fields):
                field_name = field.name()
                layer_fields.append(field_name)
                item_type = QTableWidgetItem(field_name)
                wid.tbl_fields.setItem(i, 0, item_type)
                checkbox = QTableWidgetItem()
                # Check if the field existed, otherwise add it to the configuration dictionary
                if field_name in field_dict:
                    checkbox.setData(
                        Qt.CheckStateRole,
                        Qt.Checked
                        if field_dict[field_name]["mandatory"]
                        else Qt.Unchecked,
                    )
                else:
                    checkbox.setData(
                        Qt.CheckStateRole,
                        Qt.Unchecked,
                    )
                    self.conf_dict[layer]["fields"][field_name] = {}
                    self.conf_dict[layer]["fields"][field_name]["mandatory"] = False
                    layers_fields_added_deleted_dict[layer]["fields_added"].append(
                        field_name
                    )
                checkbox.setFlags(checkbox.flags() & ~Qt.ItemIsEditable)
                wid.tbl_fields.setItem(i, 1, checkbox)

            # Check if a field has been deleted
            layers_fields_added_deleted_dict[layer]["fields_deleted"].extend(
                list(set(layer_fields) ^ set(list(field_dict)))
            )

            # Layer constraints
            wid.chbx_invalid_geom.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["invalid_geometry"])
            )
            wid.chbx_null_geom.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["null_geometry"])
            )
            wid.chbx_empty_geom.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["empty_geometry"])
            )
            wid.chbx_duplicates_geom.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["duplicates_geometry"])
            )
            wid.chbx_overlaps_geom.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["overlaps_geometry"])
            )
            if dim == 0:
                pass
            elif dim == 1:
                wid.chbx_curves_line.setCheckState(
                    boolToQtCheckState(self.conf_dict[layer]["curves"])
                )
                wid.spb_min_length.setValue(self.conf_dict[layer]["min_length"])
            elif dim == 2:
                wid.chbx_holes.setCheckState(
                    boolToQtCheckState(self.conf_dict[layer]["holes"])
                )
                wid.chbx_curves_poly.setCheckState(
                    boolToQtCheckState(self.conf_dict[layer]["curves"])
                )
                wid.spb_min_area.setValue(self.conf_dict[layer]["min_area"])
            wid.chbx_strict_geom.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["strict_geometry"])
            )
            wid.chbx_strict_crs.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["strict_crs"])
            )
            wid.chbx_strict_fields.setCheckState(
                boolToQtCheckState(self.conf_dict[layer]["strict_fields"])
            )

            wid.gbx_point.setVisible(dim == 0)
            wid.gbx_line.setVisible(dim == 1)
            wid.gbx_poly.setVisible(dim == 2)
            self.stack.addWidget(wid)
            self.layers_view.addItem(
                QListWidgetItem(ICON_DICT[geom_type.upper()], layer)
            )

        # Notification of added and deleted fields
        notification_text = []
        for layer, fields_status in layers_fields_added_deleted_dict.items():
            fields_added = fields_status["fields_added"]
            fields_deleted = fields_status["fields_deleted"]
            if fields_added or fields_deleted:
                notification_text.append(tr("- Layer {0} :").format(layer))
                if fields_added:
                    nb_fields_added = len(fields_added)
                    notification_text.append(
                        tr(" {0} new field(s) ({1}),", "", nb_fields_added).format(
                            nb_fields_added,
                            ", ".join(map(str, fields_added))
                            if nb_fields_added <= 5
                            else ", ".join(map(str, fields_added[:5]))
                            + tr(", and {0} more...").format(str(nb_fields_added - 5)),
                        )
                    )
                if fields_deleted:
                    nb_fields_deleted = len(fields_deleted)
                    notification_text.append(
                        tr(
                            " {0} field(s) deleted ({1}),", "", nb_fields_deleted
                        ).format(
                            nb_fields_deleted,
                            ", ".join(map(str, fields_deleted))
                            if nb_fields_deleted <= 5
                            else ", ".join(map(str, fields_deleted[:5]))
                            + tr(", and {0} more...").format(
                                str(nb_fields_deleted - 5)
                            ),
                        )
                    )
                notification_text.append("\n")
        if notification_text:
            QMessageBox.information(
                None,
                tr("Information"),
                tr("".join(notification_text[:-1])[:-1]),
            )

    def cleanupPage(self):
        """Called when user click on Back button"""
        for i in range(self.layers_view.count()):
            self.layers_view.takeItem(i)
            self.stack.removeWidget(self.stack.widget(i))
            self.conf_dict.clear()

    def exportConfig(self) -> None:
        """Save configuration file in YAML"""

        save_path = Path(self.field("save_path"))
        if save_path.suffix == "":
            save_path = save_path.with_suffix(".yaml")

        for i in range(self.stack.count()):
            wid = self.stack.widget(i)
            layer = self.layers_view.item(i).text()

            # Fields constraints
            for j in range(wid.tbl_fields.rowCount()):
                text_item = wid.tbl_fields.item(j, 0)
                chbx_item = wid.tbl_fields.item(j, 1)
                self.conf_dict[layer]["fields"][text_item.text()][
                    "mandatory"
                ] = qtCheckStateToBool(chbx_item.checkState())

            # Geometric and topological constraints
            self.conf_dict[layer]["invalid_geometry"] = qtCheckStateToBool(
                wid.chbx_invalid_geom.checkState()
            )
            self.conf_dict[layer]["null_geometry"] = qtCheckStateToBool(
                wid.chbx_null_geom.checkState()
            )
            self.conf_dict[layer]["empty_geometry"] = qtCheckStateToBool(
                wid.chbx_empty_geom.checkState()
            )
            self.conf_dict[layer]["duplicates_geometry"] = qtCheckStateToBool(
                wid.chbx_duplicates_geom.checkState()
            )
            self.conf_dict[layer]["overlaps_geometry"] = qtCheckStateToBool(
                wid.chbx_overlaps_geom.checkState()
            )

            # Geometry dependent contraints
            dim = QgsWkbTypes.wkbDimensions(
                QgsWkbTypes.parseType(self.conf_dict[layer]["geometry_type"])
            )
            if dim == 0:
                pass
            elif dim == 1:
                self.conf_dict[layer]["curves"] = qtCheckStateToBool(
                    wid.chbx_curves_line.checkState()
                )
                self.conf_dict[layer]["min_length"] = wid.spb_min_length.value()
            elif dim == 2:
                self.conf_dict[layer]["holes"] = qtCheckStateToBool(
                    wid.chbx_holes.checkState()
                )
                self.conf_dict[layer]["curves"] = qtCheckStateToBool(
                    wid.chbx_curves_poly.checkState()
                )
                self.conf_dict[layer]["min_area"] = wid.spb_min_area.value()

            # Generic constraints
            self.conf_dict[layer]["strict_geometry"] = qtCheckStateToBool(
                wid.chbx_strict_geom.checkState()
            )
            self.conf_dict[layer]["strict_crs"] = qtCheckStateToBool(
                wid.chbx_strict_crs.checkState()
            )
            self.conf_dict[layer]["strict_fields"] = qtCheckStateToBool(
                wid.chbx_strict_fields.checkState()
            )

        with save_path.open("w") as of:
            yaml.dump(self.conf_dict, of)
            QMessageBox.information(
                None,
                tr("Completed"),
                tr("The configuration file has been saved to {0}").format(save_path),
            )


class ConfigPage(QWizardPage):
    """
    Page to configure the verification constraints
    while creating a new configuration
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Constraints configuration"))
        page_layout = QHBoxLayout()
        self.setLayout(page_layout)
        self.stack = QStackedWidget()
        self.layers_view = QListWidget()
        self.layers_view.setMinimumWidth(150)
        self.layers_view.setMaximumWidth(300)
        page_layout.addWidget(self.layers_view)
        page_layout.addWidget(self.stack)

        self.conf_dict: Dict[str, Any] = {}

        self.layers_view.currentRowChanged.connect(self.stack.setCurrentIndex)

    def initializePage(self):
        """Initialize the page."""

        input_format = InputFormat(self.field("input_format"))
        filepath = self.field("filepath")

        # Shapefile
        if input_format == InputFormat.SHAPEFILES:
            info_list = list_shp_info(filepath)

        # CAD or DXF
        elif input_format == InputFormat.DXF:
            info_list = list_dxf_info(filepath)

        # GPKG
        elif input_format == InputFormat.GEOPACKAGE:
            info_list = list_gpkg_info(filepath)

        self.conf_dict["filepath"] = filepath
        self.conf_dict["input_format"] = input_format.value
        for layer, (geom_type, fields) in info_list.items():
            wid = uic.loadUi(Path(__file__).resolve(True).parent / "conf_wid.ui")
            wid.tbl_fields.setRowCount(len(fields))
            field_dict = {}
            for i, field in enumerate(fields):
                field_name = field.name()
                item_type = QTableWidgetItem(field_name)
                field_dict[field_name] = {
                    "mandatory": False,
                }
                wid.tbl_fields.setItem(i, 0, item_type)
                checkbox = QTableWidgetItem()
                checkbox.setData(Qt.CheckStateRole, Qt.Unchecked)
                checkbox.setFlags(checkbox.flags() & ~Qt.ItemIsEditable)
                wid.tbl_fields.setItem(i, 1, checkbox)
            self.conf_dict[layer] = {
                "fields": field_dict,
                "geometry_type": geom_type,
            }
            dim = QgsWkbTypes.wkbDimensions(QgsWkbTypes.parseType(geom_type))
            wid.gbx_point.setVisible(dim == 0)
            wid.gbx_line.setVisible(dim == 1)
            wid.gbx_poly.setVisible(dim == 2)
            self.stack.addWidget(wid)
            self.layers_view.addItem(
                QListWidgetItem(ICON_DICT[geom_type.upper()], layer)
            )

    def cleanupPage(self):
        """Called when user click on Back button"""
        for i in range(self.layers_view.count()):
            self.layers_view.takeItem(i)
            self.stack.removeWidget(self.stack.widget(i))
            self.conf_dict.clear()

    def exportConfig(self) -> None:
        """Save configuration file in YAML"""

        save_path = Path(self.field("save_path"))
        if save_path.suffix == "":
            save_path = save_path.with_suffix(".yaml")

        for i in range(self.stack.count()):
            wid = self.stack.widget(i)
            layer = self.layers_view.item(i).text()

            # Fields constraints
            for j in range(wid.tbl_fields.rowCount()):
                item = wid.tbl_fields.item(j, 0)
                chbx_item = wid.tbl_fields.item(j, 1)
                self.conf_dict[layer]["fields"][item.text()][
                    "mandatory"
                ] = qtCheckStateToBool(chbx_item.checkState())

            # Geometric and topological constraints
            self.conf_dict[layer]["invalid_geometry"] = qtCheckStateToBool(
                wid.chbx_invalid_geom.checkState()
            )
            self.conf_dict[layer]["null_geometry"] = qtCheckStateToBool(
                wid.chbx_null_geom.checkState()
            )
            self.conf_dict[layer]["empty_geometry"] = qtCheckStateToBool(
                wid.chbx_empty_geom.checkState()
            )
            self.conf_dict[layer]["duplicates_geometry"] = qtCheckStateToBool(
                wid.chbx_duplicates_geom.checkState()
            )
            self.conf_dict[layer]["overlaps_geometry"] = qtCheckStateToBool(
                wid.chbx_overlaps_geom.checkState()
            )

            # Geometry dependent contraints
            dim = QgsWkbTypes.wkbDimensions(
                QgsWkbTypes.parseType(self.conf_dict[layer]["geometry_type"])
            )
            if dim == 0:
                pass
            elif dim == 1:
                self.conf_dict[layer]["curves"] = qtCheckStateToBool(
                    wid.chbx_curves_line.checkState()
                )
                self.conf_dict[layer]["min_length"] = wid.spb_min_length.value()
            elif dim == 2:
                self.conf_dict[layer]["holes"] = qtCheckStateToBool(
                    wid.chbx_holes.checkState()
                )
                self.conf_dict[layer]["curves"] = qtCheckStateToBool(
                    wid.chbx_curves_poly.checkState()
                )
                self.conf_dict[layer]["min_area"] = wid.spb_min_area.value()

            # Generic constraints
            self.conf_dict[layer]["strict_geometry"] = qtCheckStateToBool(
                wid.chbx_strict_geom.checkState()
            )
            self.conf_dict[layer]["strict_crs"] = qtCheckStateToBool(
                wid.chbx_strict_crs.checkState()
            )
            self.conf_dict[layer]["strict_fields"] = qtCheckStateToBool(
                wid.chbx_strict_fields.checkState()
            )

        with save_path.open("w") as of:
            yaml.dump(self.conf_dict, of)
            QMessageBox.information(
                None,
                tr("Completed"),
                tr("The configuration file has been saved to {0}").format(save_path),
            )


class SaveConfigPage(QWizardPage):
    """
    Page to save a configuration (while editing an existing configuration
    or creating a new one)
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Save configuration file"))
        self.setSubTitle(tr("Choose the configuration file location and name"))
        page_layout = QVBoxLayout()
        self.setLayout(page_layout)
        self.file_widget = QgsFileWidget()
        self.file_widget.setFilter("YAML (*.yaml)")
        self.file_widget.setStorageMode(QgsFileWidget.SaveFile)
        page_layout.addWidget(self.file_widget)
        self.registerField("save_path*", self.file_widget.lineEdit())

    def initializePage(self):
        """Initialize the page."""

        self.file_widget.lineEdit().setText(self.field("config_file"))

    def validatePage(self):
        """We want the extension to be empty or .yaml"""

        filepath = Path(self.field("save_path"))
        if filepath.suffix not in [".yaml", ".YAML", ""]:
            QMessageBox.warning(
                None, tr("Warning"), tr("The file extension must be .yaml")
            )
            return False
        return True


class ConfigInputPage(QWizardPage):
    """
    Page to load a configuration file to edit the configuration
    or to launch a verification
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        file_widget = QgsFileWidget()
        file_widget.setFilter("YAML (*.yaml)")
        self.setLayout(QVBoxLayout())
        self.layout().addWidget(file_widget)
        self.setTitle(tr("Load the configuration file"))
        self.registerField("config_file*", file_widget.lineEdit())

    def validatePage(self):
        """Validate the Page."""
        config_file = Path(self.field("config_file"))
        if not config_file.exists():
            QMessageBox.warning(
                None,
                tr("Warning"),
                tr("The configuration file {0} does not exist!").format(config_file),
            )
            return False

        with Path(self.field("config_file")).open() as filepath:
            conf_dict = yaml.full_load(filepath)

        check = check_conf_dict_format(conf_dict)
        if check != "":
            QMessageBox.warning(None, tr("Bad configuration file!"), check)
            return False

        ref_filepath = conf_dict["filepath"]
        if not Path(ref_filepath).exists():
            ret = QMessageBox.critical(
                None,
                tr("Error"),
                tr(
                    "The path {0} referenced in the config file"
                    " does not exist!\n"
                    "Do you want to edit the path of the file/folder?"
                ).format(ref_filepath),
                buttons=QMessageBox.Yes | QMessageBox.No,
            )
            if ret == QMessageBox.Yes:
                filepath_wdg = QgsFileWidget()
                input_format = InputFormat(conf_dict["input_format"])
                dlg = QDialog()
                dlg.setWindowTitle(tr("Choose a file"))
                if input_format == InputFormat.SHAPEFILES:
                    filepath_wdg.setStorageMode(QgsFileWidget.StorageMode.GetDirectory)
                    dlg.setWindowTitle(tr("Choose a folder"))
                else:
                    filepath_wdg.setStorageMode(QgsFileWidget.StorageMode.GetFile)
                    filepath_wdg.setFilter(conf_dict["input_format"])
                dlg_bbox = QDialogButtonBox(
                    QDialogButtonBox.Ok | QDialogButtonBox.Cancel
                )
                dlg_bbox.accepted.connect(dlg.accept)
                dlg_bbox.rejected.connect(dlg.reject)
                hbox = QVBoxLayout()
                hbox.addWidget(filepath_wdg)
                hbox.addWidget(dlg_bbox)
                dlg.setLayout(hbox)
                if dlg.exec_() == QDialog.Accepted:
                    conf_dict["filepath"] = filepath_wdg.filePath()
                    with Path(self.field("config_file")).open("w") as of:
                        yaml.dump(conf_dict, of)
                    return True
                return False
            return False

        return True


class VerifPage(QWizardPage):
    """
    Page to perform the verification
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setLayout(QVBoxLayout())
        self.text_report = QTextEdit()
        self.text_report.setReadOnly(True)
        self.layout().addWidget(self.text_report)
        self.setTitle(tr("Report"))

    def initializePage(self):
        """Initialize the page."""
        self.text_report.clear()
        with Path(self.field("config_file")).open() as filepath:
            conf_dict = yaml.full_load(filepath)
        self.worker = DoVerif(conf_dict, self.field("filepath"))
        self.worker.log_msg.connect(self.text_report.append)
        QgsApplication.taskManager().addTask(self.worker)


class SaveReportPage(QWizardPage):
    """
    Page to save the verification report
    """

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent)
        self.setTitle(tr("Save report"))
        self.setSubTitle(tr("Choose the path and format of the report"))
        page_layout = QVBoxLayout()
        self.setLayout(page_layout)
        report_path = QgsFileWidget()
        report_path.setStorageMode(QgsFileWidget.SaveFile)
        cbx_report_format = QComboBox()
        cbx_report_format.addItems(["", "Markdown", "HTML", "JSON"])
        label_report_format = QLabel(tr("Report format:"))
        page_layout.addWidget(label_report_format)
        page_layout.addWidget(cbx_report_format)
        label_report_path = QLabel(tr("Report path:"))
        page_layout.addWidget(label_report_path)
        page_layout.addWidget(report_path)
        css_path = QgsFileWidget()
        css_path.setStorageMode(QgsFileWidget.GetFile)
        css_path.setFilter("CSS (*.css)")
        label_css_path = QLabel(tr("CSS path (optional):"))
        page_layout.addWidget(label_css_path)
        page_layout.addWidget(css_path)
        self.registerField("report_format*", cbx_report_format)
        self.registerField("report_path*", report_path.lineEdit())
        self.registerField("css_path", css_path.lineEdit())

        def textChanged(text: str) -> None:
            """
            Change the behavior or the file selector according to
            the file type
            """
            label_css_path.setVisible(text == "HTML")
            css_path.setVisible(text == "HTML")
            if text == "Markdown":
                report_path.setFilter("Markdown (*.md)")
            elif text == "HTML":
                report_path.setFilter("HTML (*.html)")
            elif text == "JSON":
                report_path.setFilter("JSON (*.json)")
            elif text == "":
                report_path.setFilter("")
            else:
                raise ValueError(text + "is not a valid input")

        textChanged(cbx_report_format.currentText())
        cbx_report_format.currentTextChanged.connect(textChanged)

    def save(self, report):
        """Save the report.

        :param report: type of report format (Markdown or HTML)
        """
        with Path(self.field("report_path")).open("w") as fileout:
            if self.field("report_format") == 1:
                fileout.write(report.report(ReportFormat.MARKDOWN))
            elif self.field("report_format") == 2:
                fileout.write(report.report(ReportFormat.HTML, self.field("css_path")))
            elif self.field("report_format") == 3:
                fileout.write(report.report(ReportFormat.JSON))
            else:
                raise ValueError(self.field("report_format") + "is not a valid input")
        QMessageBox.information(
            None,
            tr("Finished"),
            tr('The report has correctly been saved to <a href="')
            + Path(self.field("report_path")).as_posix()
            + f'">{self.field("report_path")}</a>',
        )


class ConfigurationCreationWizard(QWizard):
    """
    Wizard to create a configuration file
    """

    def __init__(self, parent=Optional[QWidget]):
        super().__init__()
        self.setWindowTitle(tr("Configuration creation"))
        data_input_page = DataInputPage()
        config_page = ConfigPage()
        save_page = SaveConfigPage()
        self.addPage(data_input_page)
        self.addPage(config_page)
        self.addPage(save_page)
        self.accepted.connect(config_page.exportConfig)


class ConfigurationEditWizard(QWizard):
    """
    Wizard to edit an existing configuration file
    """

    def __init__(self, parent=Optional[QWidget]):
        super().__init__()
        self.setWindowTitle(tr("Configuration edition"))
        input_page = ConfigInputPage()
        input_page.setButtonText(QWizard.NextButton, tr("Edit configuration"))
        config_page = ConfigEditPage()
        save_page = SaveConfigPage()
        self.addPage(input_page)
        self.addPage(config_page)
        self.addPage(save_page)
        self.accepted.connect(config_page.exportConfig)


class CheckComplianceWizard(QWizard):
    """
    Wizard to perform a verification
    """

    def __init__(self, parent=Optional[QWidget]):
        super().__init__()
        self.setWindowTitle(tr("Check compliance"))
        config_page = ConfigInputPage()
        data_input_page = VerifDataInputPage()
        data_input_page.setButtonText(QWizard.NextButton, tr("Check compliance"))
        reporting_page = VerifPage()
        reporting_page.setButtonText(QWizard.NextButton, tr("Save the report"))
        reporting_page.setButtonText(QWizard.CancelButton, tr("Done"))
        save_report_page = SaveReportPage()
        self.addPage(config_page)
        self.addPage(data_input_page)
        self.addPage(reporting_page)
        self.addPage(save_report_page)
        self.accepted.connect(
            lambda: save_report_page.save(reporting_page.worker.run_report)
        )
