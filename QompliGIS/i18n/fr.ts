<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../qompligis.py" line="58"/>
        <source>Create a template</source>
        <translation>Créer une configuration</translation>
    </message>
    <message>
        <location filename="../qompligis.py" line="64"/>
        <source>Edit a template</source>
        <translation>Éditer une configuration</translation>
    </message>
    <message>
        <location filename="../qompligis.py" line="70"/>
        <source>Compliance check</source>
        <translation>Vérification de conformité</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="672"/>
        <source>Choose a file</source>
        <translation>Choisir un fichier</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="675"/>
        <source>Choose a folder</source>
        <translation>Choisir un dossier</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="93"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="641"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="442"/>
        <source>Constraints configuration</source>
        <translation>Configuration des contraintes</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="580"/>
        <source>Completed</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="595"/>
        <source>Save configuration file</source>
        <translation>Enregistrer le fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="596"/>
        <source>Choose the configuration file location and name</source>
        <translation>Choisir l'emplacement et le nom du fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="615"/>
        <source>The file extension must be .yaml</source>
        <translation>L'extension doit être .yaml</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="634"/>
        <source>Load the configuration file</source>
        <translation>Charger le fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="653"/>
        <source>Bad configuration file!</source>
        <translation>Fichier de configuration invalide !</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="658"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../report.py" line="201"/>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="729"/>
        <source>Save report</source>
        <translation>Enregistrer le rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="730"/>
        <source>Choose the path and format of the report</source>
        <translation>Choisir l'emplacement et le format du rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="788"/>
        <source>Finished</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="804"/>
        <source>Configuration creation</source>
        <translation>Création de la configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="67"/>
        <source>Choose the dataset to use as a reference</source>
        <translation>Choisir les données à utiliser comme référence</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="821"/>
        <source>Configuration edition</source>
        <translation>Modification de la configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="823"/>
        <source>Edit configuration</source>
        <translation>Modifier la configuration</translation>
    </message>
    <message>
        <location filename="../processing.py" line="133"/>
        <source>Check compliance</source>
        <translation>Vérifier la conformité</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="124"/>
        <source>Choose the dataset to check</source>
        <translation>Choisir les données à vérifier</translation>
    </message>
    <message>
        <location filename="../utils.py" line="684"/>
        <source>The path {0} does not exist!</source>
        <translation>Le chemin {0} n'existe pas !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="727"/>
        <source>No layer found in reference file!</source>
        <translation>Pas de couche trouvée dans le fichier de référence !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="747"/>
        <source>Checking layer list...</source>
        <translation>Vérification de la liste de couches...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="750"/>
        <source>OK: Layers comply with the reference</source>
        <translation>OK : les couches sont conformes à la référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="757"/>
        <source>Layer &apos;{0}&apos; is missing in dataset</source>
        <translation>La couche '{0}' est absente des données d'entrée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="761"/>
        <source>Layer &apos;{0}&apos; found but not present in reference dataset</source>
        <translation>La couche '{0}' a été trouvée, mais est absente des données de référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="777"/>
        <source>Checking layer {0}</source>
        <translation>Vérification de la couche {0}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="781"/>
        <source>Checking CRS...</source>
        <translation>Vérification du SCR...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1152"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../utils.py" line="788"/>
        <source>CRS is {new_crs} but must be {ref_crs}</source>
        <translation>Le SRC est {new_crs} mais doit être {ref_crs}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="795"/>
        <source>No CRS check asked</source>
        <translation>Pas de vérification du SCR demandée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="801"/>
        <source>Checking fields...</source>
        <translation>Vérification des champs...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="813"/>
        <source>Differences found on fields: see each field information.</source>
        <translation>Différences trouvées sur les champs : voir le détail de chaque champ</translation>
    </message>
    <message>
        <location filename="../utils.py" line="818"/>
        <source>Missing</source>
        <translation>Manquant</translation>
    </message>
    <message>
        <location filename="../utils.py" line="822"/>
        <source> - AND MANDATORY!</source>
        <translation> - ET OBLIGATOIRE !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="824"/>
        <source>Not in reference layer.</source>
        <translation>Absent de la couche de référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="828"/>
        <source>Fields are in a different order: found {0} but reference layer order is {1}.</source>
        <translation>Les champs sont dans un ordre différent : {0} trouvé mais l'ordre de référence est {1}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="832"/>
        <source>Fields are all present and in the same order: see each field information.</source>
        <translation>Les champs sont tous présents et dans le même ordre : voir le détail de chaque champ.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="841"/>
        <source>Length differ: found {0} but reference is {1}.</source>
        <translation>La longueur diffère : trouvé {0} mais la référence est {1}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="846"/>
        <source>Precision differ: found {0} but reference is {1}.</source>
        <translation>La précision diffère : trouvée {0} mais la référence est {1}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="851"/>
        <source>Type differ: found (type: {0}, length: {1}, precision: {2}) but reference is (type: {3}, length: {4}, precision: {5}).</source>
        <translation>Le type diffère : trouvé (type: {0}, longueur: {1}, précision: {2}) mais la référence est (type: {3}, longueur: {4}, précision: {5}).</translation>
    </message>
    <message>
        <location filename="../utils.py" line="864"/>
        <source>Other difference... found informations are type: {0}, length: {1}, precision: {2} and reference informations are type: {3}, length: {4}, precision: {5}.</source>
        <translation>Autre différence... informations trouvées : type: {0}, longueur: {1}, précision: {2} et les informations de référence sont : type: {3}, longueur: {4}, précision: {5}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="876"/>
        <source>Fields are the same.</source>
        <translation>Les champs sont identiques.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="881"/>
        <source>No strict check on fields</source>
        <translation>Pas de vérification stricte sur les champs.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="966"/>
        <source>Checking minimum length...</source>
        <translation>Vérification de la longueur minimale...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="969"/>
        <source>No minimum length check asked</source>
        <translation>Pas de vérification de longueur minimale demandée.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="975"/>
        <source>Checking minimum area...</source>
        <translation>Vérification de l'aire minimale...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="978"/>
        <source>No minimum area check asked</source>
        <translation>Pas de vérification d'aire minimale demandée.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="984"/>
        <source>Checking curves...</source>
        <translation>Vérification des courbes...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="986"/>
        <source>No curves check asked</source>
        <translation>Pas de vérification de courbes demandée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="992"/>
        <source>Checking holes...</source>
        <translation>Vérification des trous...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="994"/>
        <source>No holes check asked</source>
        <translation>Pas de vérification de trous demandée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1056"/>
        <source>Feature {0} has a length {1} &lt; minimum length {2}</source>
        <translation>L'entité {0} a une longueur de {1} &amp;lt; longueur minimum {2}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1066"/>
        <source>Feature {0} has an area {1} &lt; minimum area {2}</source>
        <translation>L'entité {0} a une aire de {1} &amp;lt; aire minimum {2}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1076"/>
        <source>Feature {0} contains curve(s)</source>
        <translation>L'entité {0} contient une (des) courbe(s)</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1084"/>
        <source>Feature {0} contains hole(s)</source>
        <translation>L'entité {0} contient un (des) trou(s)</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1140"/>
        <source>OK: layer complies with minimum length</source>
        <translation>OK : la couche est conforme à la longueur minimum</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1145"/>
        <source>OK: layer complies with minimum area</source>
        <translation>OK : la couche est conforme à l'aire minimum</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1150"/>
        <source>OK: layer does not have curve</source>
        <translation>OK : la couche n'a pas de courbe</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1155"/>
        <source>OK: layer does not have hole</source>
        <translation>OK : la couche n'a pas de trou</translation>
    </message>
    <message>
        <location filename="../report.py" line="52"/>
        <source>Layer list</source>
        <translation>Liste de couches</translation>
    </message>
    <message>
        <location filename="../report.py" line="54"/>
        <source>CRS</source>
        <translation>SCR</translation>
    </message>
    <message>
        <location filename="../report.py" line="55"/>
        <source>Fields</source>
        <translation>Champs</translation>
    </message>
    <message>
        <location filename="../report.py" line="56"/>
        <source>Curves</source>
        <translation>Courbes</translation>
    </message>
    <message>
        <location filename="../report.py" line="57"/>
        <source>Holes</source>
        <translation>Trous</translation>
    </message>
    <message>
        <location filename="../report.py" line="58"/>
        <source>Minimum length</source>
        <translation>Longueur minimum</translation>
    </message>
    <message>
        <location filename="../report.py" line="59"/>
        <source>Minimum area</source>
        <translation>Aire minimum</translation>
    </message>
    <message>
        <location filename="../report.py" line="97"/>
        <source>For FIELDS section, use addFieldInfo() method</source>
        <translation>Pour la section FIELDS, utiliser la méthode addFieldInfo()</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="138"/>
        <source>Choose a folder with your shapefiles</source>
        <translation>Choisir un dossier avec vos shapefiles</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="141"/>
        <source>Choose a file - {0}</source>
        <translation>Choisir un fichier - {0}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1243"/>
        <source>Bad formed configuration file: missing section(s) {0}</source>
        <translation>Fichier de configuration malformé : section(s) manquante(s) {0}</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="580"/>
        <source>The configuration file has been saved to {0}</source>
        <translation>Le fichier de configuration a été enregistré vers {0}</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="641"/>
        <source>The configuration file {0} does not exist!</source>
        <translation>Le fichier de configuration {0} n'existe pas !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="705"/>
        <source>The data format to check ({0}) does not correspond with the reference data ({1})</source>
        <translation>Le format des données à vérifier ({0}) ne correspond pas aux données de référence ({1})</translation>
    </message>
    <message>
        <location filename="../report.py" line="103"/>
        <source>The layer {0} is not present in the report</source>
        <translation>La couche {0} est absente dans le rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="28"/>
        <source>Configuration file</source>
        <translation>Fichier de configuration</translation>
    </message>
    <message>
        <location filename="../processing.py" line="37"/>
        <source>Data folder to check (for Shapefiles)</source>
        <translation>Dossier de données à vérifier (pour Shapefiles)</translation>
    </message>
    <message>
        <location filename="../processing.py" line="46"/>
        <source>Data file to check</source>
        <translation>Fichier de données à vérifier</translation>
    </message>
    <message>
        <location filename="../processing.py" line="55"/>
        <source>Report format</source>
        <translation>Format du rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="64"/>
        <source>Report path</source>
        <translation>Chemin du rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="93"/>
        <source>A data file or a data folder to check (for Shapefiles) must be supplied</source>
        <translation>Un fichier de données ou un dossier de données à vérifier (pour Shapefiles) doit être renseigné</translation>
    </message>
    <message>
        <location filename="../processing.py" line="119"/>
        <source>Saved to </source>
        <translation>Sauvegardé vers </translation>
    </message>
    <message>
        <location filename="../utils.py" line="826"/>
        <source>Field {0}: {1}</source>
        <translation>Champ {0} : {1}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1000"/>
        <source>Checking NULL fields...</source>
        <translation>Vérification des champs NULL...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1104"/>
        <source>Mandatory field is NULL in feature {0}</source>
        <translation>Champ obligatoire à NULL dans l'entité {0}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1173"/>
        <source>The dataset does not comply with the constraints (see details in report)</source>
        <translation>Les données ne sont pas conformes aux contraintes (voir détails dans le rapport)</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1177"/>
        <source>The dataset comply with the constraints</source>
        <translation>Les données sont conformes aux contraintes</translation>
    </message>
    <message>
        <location filename="../report.py" line="52"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="158"/>
        <source>The path {0} must be a folder!</source>
        <translation>Le chemin {0} doit être un dossier !</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="166"/>
        <source>The path {0} must be a file!</source>
        <translation>Le chemin {0} doit être un fichier !</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="844"/>
        <source>Save the report</source>
        <translation>Enregistrer le rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="845"/>
        <source>Done</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="658"/>
        <source>The path {0} referenced in the config file does not exist!
Do you want to edit the path of the file/folder?</source>
        <translation>Le chemin {0} référencé dans le fichier de configuration n'existe pas !
Voulez-vous éditer le chemin du fichier/dossier ?</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="316"/>
        <source>- Layer {0} :</source>
        <translation>- Couche {0} :</translation>
    </message>
    <message numerus="yes">
        <location filename="../gui_utils.py" line="319"/>
        <source> {0} new field(s) ({1}),</source>
        <translation>
            <numerusform> {0} nouveau champ ({1}),</numerusform>
            <numerusform> {0} nouveaux champs ({1}),</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../gui_utils.py" line="330"/>
        <source> {0} field(s) deleted ({1}),</source>
        <translation>
            <numerusform> {0} champ supprimé ({1}),</numerusform>
            <numerusform> {0} champs supprimés ({1}),</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="345"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../report.py" line="201"/>
        <source>Compliance Report</source>
        <translation>Rapport de vérification</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="330"/>
        <source>, and {0} more...</source>
        <translation>, et {0} de plus...</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="737"/>
        <source>Report format:</source>
        <translation>Format du rapport :</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="740"/>
        <source>Report path:</source>
        <translation>Chemin du rapport :</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="746"/>
        <source>CSS path (optional):</source>
        <translation>Chemin du fichier CSS (optionnel)</translation>
    </message>
    <message>
        <location filename="../processing.py" line="71"/>
        <source>CSS file path (for HTML report)</source>
        <translation>Chemin du fichier CSS (pour rapport HTML)</translation>
    </message>
    <message>
        <location filename="../utils.py" line="926"/>
        <source>Checking geometries validity...</source>
        <translation>Vérification de la validité des géométries...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="928"/>
        <source>No geometries validity check asked</source>
        <translation>Pas de vérification demandée sur la validité géométrique</translation>
    </message>
    <message>
        <location filename="../utils.py" line="950"/>
        <source>Checking geometry duplicates...</source>
        <translation>Vérification des duplicats de géomtrie...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="952"/>
        <source>No geometry duplicates check asked</source>
        <translation>Pas de vérification demandée sur les duplicats de géométrie</translation>
    </message>
    <message>
        <location filename="../utils.py" line="960"/>
        <source>No geometry overlaps check asked</source>
        <translation>Pas de vérification demandée sur les superpositions de géométrie</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1115"/>
        <source>OK: layer has no invalid geometry</source>
        <translation>OK: la couche n'a aucune géométrie invalide</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1130"/>
        <source>OK: layer has no geometry duplicate</source>
        <translation>OK: la couche n'a pas de duplicat de géométrie</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1135"/>
        <source>OK: layer has no geometry overlay</source>
        <translation>OK: la couche n'a pas de superposition de géométrie</translation>
    </message>
    <message>
        <location filename="../report.py" line="60"/>
        <source>Invalid geometries</source>
        <translation>Géométries invalides</translation>
    </message>
    <message>
        <location filename="../report.py" line="63"/>
        <source>Duplicate geometries</source>
        <translation>Duplicats de géométrie</translation>
    </message>
    <message>
        <location filename="../report.py" line="64"/>
        <source>Overlaps geometries</source>
        <translation>Superpositions de géométrie</translation>
    </message>
    <message>
        <location filename="../utils.py" line="958"/>
        <source>Checking geometry overlays...</source>
        <translation>Vérification de la superposition des géométries...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="934"/>
        <source>Checking null geometries...</source>
        <translation>Vérification des géométries nulles...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="936"/>
        <source>No null geometries check asked</source>
        <translation>Pas de vérification sur les géométries nulles demandées</translation>
    </message>
    <message>
        <location filename="../utils.py" line="944"/>
        <source>No empty geometries check asked</source>
        <translation>Pas de vérification sur les géométries vides demandées</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1120"/>
        <source>OK: layer has no null geometry</source>
        <translation>OK: La couche n'a pas de géométrie nulle</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1125"/>
        <source>OK: layer has no empty geometry</source>
        <translation>OK: La couche n'a pas de géométrie vide</translation>
    </message>
    <message>
        <location filename="../report.py" line="61"/>
        <source>Null geometries</source>
        <translation>Géométries nulles</translation>
    </message>
    <message>
        <location filename="../report.py" line="62"/>
        <source>Empty geometries</source>
        <translation>Géométries vides</translation>
    </message>
    <message>
        <location filename="../utils.py" line="942"/>
        <source>Checking empty geometries...</source>
        <translation>Vérification des géométries vides...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1014"/>
        <source>Feature {0} has an null geometry</source>
        <translation>L'entité {0} a une géométrie nulle</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1022"/>
        <source>Feature {0} has an empty geometry</source>
        <translation>L'entité {0} a une géométrie vide</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1033"/>
        <source>Feature {0} is identical to the features : {1}</source>
        <translation>L'entité {0} est identique aux entités : {1}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1046"/>
        <source>Feature {0} overlaps the features : {1}</source>
        <translation>L'entité {0} superpose les entités : {1}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="1006"/>
        <source>Feature {0} has an invalid geometry</source>
        <translation>L'entité {0} a une géométrie invalide</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="788"/>
        <source>The report has correctly been saved to &lt;a href=&quot;</source>
        <translation>Le rapport a bien été sauvegardé ici &lt;a href=&quot;</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../conf_wid.ui" line="14"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="20"/>
        <source>Which types of compliances do you want to configure ?</source>
        <translation>Quelles types de conformités voulez vous configurer ?</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="27"/>
        <source>Strict geometry type (Multi vs single, Z, M, etc)</source>
        <translation>Type de géométrie strict (Multi vs single, Z, M, etc)</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="37"/>
        <source>Don&apos;t allow a different coordinate reference system</source>
        <translation>Ne pas autoriser un système de coordonnées différent</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="44"/>
        <source>Don&apos;t allow new attributes</source>
        <translation>Ne pas autoriser de nouveaux attributs</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="52"/>
        <source>Attributes</source>
        <translation>Attributs</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="57"/>
        <source>Mandatory</source>
        <translation>Obligatoire</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="109"/>
        <source>Point layer options</source>
        <translation>Options de couche type Point</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="115"/>
        <source>No option yet</source>
        <translation>Pas d'option pour le moment</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="125"/>
        <source>Line layer options</source>
        <translation>Options de couche type Ligne</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="182"/>
        <source>Can contain curves</source>
        <translation>Peut contenir des courbes</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="138"/>
        <source>Minimum length</source>
        <translation>Longueur minimale</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="155"/>
        <source>Polygon layer options</source>
        <translation>Option de couche type Polygone</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="161"/>
        <source>Minimum area</source>
        <translation>Aire minimum</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="175"/>
        <source>Can contain holes</source>
        <translation>Peut contenir des trous</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="71"/>
        <source>Can contain invalid geometries</source>
        <translation>Peut contenir des géométries invalides</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="85"/>
        <source>Can contain duplicates</source>
        <translation>Peut contenir des duplicats de géométrie</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="78"/>
        <source>Can contain overlaps</source>
        <translation>Peut contenir des superpositions de géométrie</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="65"/>
        <source>Geometry and topology options</source>
        <translation>Options de géométrie et de topologie</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="92"/>
        <source>Can contain null geometries</source>
        <translation>Peut contenir des géométries nulles</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="99"/>
        <source>Can contain empty geometries</source>
        <translation>Peut contenir des géométries vides</translation>
    </message>
</context>
</TS>
